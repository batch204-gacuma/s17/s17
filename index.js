/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function personalInfo() {
	let fullName = prompt("Enter Your Fullname:");
	let age = prompt("Enter your age:");
	let loc = prompt("Enter your location:");

	console.log("Hello, "+fullName);
	console.log("You are "+age+" years old");
	console.log("You live in "+loc);

}
personalInfo();



/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function band() {
		const band1 = "1. AESPA";
		const band2 = "2. BLACKPINK";
		const band3 = "3. TWICE";
		const band4 = "4. LAPILLUS";
		const band5 = "5. LOONA";

		console.log(band1);
		console.log(band2);
		console.log(band3);
		console.log(band4);
		console.log(band5);


	}
	band();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function movie() {
		const movie1 = "1. Scary Movie 1";
		const r1 = 'Rotten tomato rating: 52%';
		const movie2 = "2. Scary Movie 2";
		const r2 = 'Rotten tomato rating: 52%';
		const movie3 = "3. Scary Movie 3";
		const r3 = 'Rotten tomato rating: 52%';
		const movie4 = "4. Scary Movie 4";
		const r4 = 'Rotten tomato rating: 52%';
		const movie5 = "5. Scary Movie 5";
		const r5 = 'Rotten tomato rating: 52%'

		console.log(movie1);
		console.log(r1);
		console.log(movie2);
		console.log(r2);
		console.log(movie3);
		console.log(r3);
		console.log(movie4);
		console.log(r4);
		console.log(movie5);
		console.log(r5);

	}
	movie();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printUsers();

// console.log(friend1);
// console.log(friend2);